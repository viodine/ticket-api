# Ticket API

## Recruitment Task

'App' has to main features:

- Displaying information about available events (name, datetime, number of available tickets, tickets data).
- Simulating payment process for a ticket.

Endpoints:

- GET /events => displays basic info about all events in db.
  - Sample response:
    ```json
    [
      {
          "id": 1,
          "name": "The Mars Volta Concert",
          "datetime": "2020-07-29T01:25:33.000Z"
      },
      {
          "id": 2,
          "name": "O.A.R. Concert",
          "datetime": "2020-06-19T17:58:57.000Z"
      },
      ...
    ]
    ```
- GET /events/:id => displays details about single event and available tickets
  - Sample response:
    ```json
    {
      "event": {
        "id": 3,
        "name": "O.A.R. Concert",
        "datetime": "2020-04-24T02:19:42.000Z",
        "available": 35,
        "tickets": [
            {
                "id": 9,
                "sector": "l",
                "row": 38,
                "seat": 14,
                "price": "31.21"
            },
            {
                "id": 15,
                "sector": "o",
                "row": 43,
                "seat": 3,
                "price": "59.98"
            },
            ...
        ]
      }
    }
    ```
- POST /tickets => takes object with ids of tickets user want to buy and returns info message and transaction price when everything went well or error message when something went wrong.
  - Sample request body:
    ```json
    {
      "tickets": [{ "id": 41 }, { "id": 256 }]
    }
    ```
  - 'Positive' response
    ```json
    {
      "info": {
        "message": "Transaction was successful.",
        "price": "115.33"
      }
    }
    ```
  - PaymentError response
    ```json
    {
      "info": {
        "message": "Something went wrong with your transaction.",
        "price": null
      }
    }
    ```
  - CardError response
    ```json
    {
      "info": {
        "message": "Your card has been declined.",
        "price": null
      }
    }
    ```
  - Unavailable seats response
    ```json
    {
      "info": {
        "message": "Seat 9, Row 16 in sector e is unavailable. Try another one.",
        "price": null
      }
    }
    ```
