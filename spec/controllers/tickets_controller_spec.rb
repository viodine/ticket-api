# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TicketsController, type: :controller do
  render_views
  let(:json) { JSON.parse(response.body) }
  let!(:tickets) { create_list(:ticket, 100) }
  let!(:unavailable_tickets) { create_list(:unavailable_ticket, 100) }

  describe 'POST /tickets' do
    context 'when the request contains multiple (available) tickets' do
      let(:ticket_1_id) { tickets.sample.id } 
      let(:ticket_2_id) { tickets.sample.id } 
      let(:multiple_tickets) { { tickets: [{ id: ticket_1_id }, { id: ticket_2_id }] } }
      before { 
        allow_any_instance_of(TicketsController).to receive(:simulate_transaction_check).and_return('')
        post :create, params: multiple_tickets, format: :json
      }

      it 'book seats' do
        expect(json['info']['message']).to eq('Transaction was successful.')
        expect(json['info']['price']).to eq((Ticket.find(ticket_1_id).price + Ticket.find(ticket_2_id).price).to_f.to_s)
      end
    end

    context 'when the request contains single (available) ticket' do
      let(:ticket_id) { tickets.sample.id }
      let(:ticket) { { tickets: [{ id: ticket_id }] } }
      before { 
        allow_any_instance_of(TicketsController).to receive(:simulate_transaction_check).and_return('')
        post :create, params: ticket, format: :json
      }

      it 'book a seat' do
        expect(json['info']['message']).to eq('Transaction was successful.')
        expect(json['info']['price']).to eq(Ticket.find(ticket_id).price.to_f.to_s)
      end
    end

    context 'when the request contains single (unavailable) ticket' do
      let(:unavailable_ticket_id) { unavailable_tickets.sample.id }
      let(:single_unavailable_ticket) { { tickets: [{ id: unavailable_ticket_id }] } }
      before { post :create, params: single_unavailable_ticket, format: :json }

      it 'displays error message without charge' do
        expect(json['info']['message']).to eq("Seat #{Ticket.find(unavailable_ticket_id).seat}, Row #{Ticket.find(unavailable_ticket_id).row} in sector #{Ticket.find(unavailable_ticket_id).sector} is unavailable. Try another one.")
        expect(json['info']['price']).to eq(nil)
      end
    end

    context 'when the request contains multiple (unavailable) tickets' do
      let(:multiple_unavailable_tickets) { { tickets: [{ id: unavailable_tickets[10].id }, { id: unavailable_tickets[11].id }] } }
      before { post :create, params: multiple_unavailable_tickets, format: :json }

      it 'displays error message without charge' do
        expect(json['info']['message']).to eq("Seat #{Ticket.find(unavailable_tickets[10].id).seat}, Row #{Ticket.find(unavailable_tickets[10].id).row} in sector #{Ticket.find(unavailable_tickets[10].id).sector} is unavailable. Try another one.")
        expect(json['info']['price']).to eq(nil)        
      end
    end

    context 'when the token informs about CardError' do
      let(:ticket) { { tickets: [{ id: tickets.sample.id }] } }
      before { 
        allow_any_instance_of(TicketsController).to receive(:simulate_transaction_check).and_return('card_error')
        post :create, params: ticket, format: :json
      }

      it 'displays appropriate message' do
        expect(json['info']['message']).to eq('Your card has been declined.')
        expect(json['info']['price']).to eq(nil)
      end
    end

    context 'when the token informs about PaymentError' do
      let(:ticket) { { tickets: [{ id: tickets.sample.id }] } }
      before { 
        allow_any_instance_of(TicketsController).to receive(:simulate_transaction_check).and_return('payment_error')
        post :create, params: ticket, format: :json
      }

      it 'displays appropriate message' do
        expect(json['info']['message']).to eq('Something went wrong with your transaction.')
        expect(json['info']['price']).to eq(nil)
      end
    end
  end
end
