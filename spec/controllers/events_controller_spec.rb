# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  render_views

  let(:json) { JSON.parse(response.body) }
  let!(:events) { create_list(:event_with_tickets, 10) }
  let(:event_id) { events.sample.id }

  describe 'GET /events' do
    before {
      get :index, format: :json
    }

    it 'returns all events' do
      expect(json.length).to eq(10)
    end

    it 'each event contains id, name and datetime keys' do
      expect(json).to all(have_key('id'))
      expect(json).to all(have_key('name'))
      expect(json).to all(have_key('datetime'))
    end
  end

  describe 'GET /events/:id' do
    context 'when all ticket are available' do
      before {
        get :show, params: { id: event_id }, format: :json
      }
      it 'returns event info' do
        expect(json['event']['id']).to eq(Event.find(event_id).id)
        expect(json['event']['name']).to eq(Event.find(event_id).name)
        expect(json['event']['datetime']).to eq(Event.find(event_id).datetime.iso8601(3))
      end
    end

    context 'when some tickets are unavailable' do
      before {
        Event.find(event_id).tickets[0..10].each { |t| t.update(is_paid: true) }
        get :show, params: { id: event_id }, format: :json
      }
      it 'returns all available tickets for event' do
        expect(json['event']['tickets'].length).to eq(Event.find(event_id).tickets.reject(&:is_paid).length)
        expect(json['event']['tickets']).to all(include('id', 'sector', 'row', 'seat', 'price'))
      end
    end
  end
end
