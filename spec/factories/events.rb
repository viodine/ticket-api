# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    name { Faker::Music.band.gsub('\u0000', '') }
    datetime { Faker::Time.forward }

    factory :event_with_tickets do
      after :create do |event|
        create_list :ticket, 300, event: event
      end
    end
  end
end
