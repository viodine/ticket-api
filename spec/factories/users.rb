# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    authentication_token { Faker::String.random(length: 30) }

    factory :user_with_tickets do
      after :create do |user|
        create_list :ticket, 5, user: user
      end
    end
  end
end
