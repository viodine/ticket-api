# frozen_string_literal: true

FactoryBot.define do
  factory :ticket do
    price { Faker::Number.decimal(l_digits: 2, r_digits: 2) }
    sector { Faker::Types.character }
    row { Faker::Number.between(from: 1, to: 500) }
    seat { Faker::Number.between(from: 1, to: 500) }
    event
    # user

    factory :unavailable_ticket do
      is_paid { true }
    end
  end
end
