# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:event) { create(:event) }
  let(:event_with_tickets) { create(:event_with_tickets) }

  describe 'validations' do
    subject { event }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:datetime) }
  end

  describe 'has tickets' do
    subject { event_with_tickets }
    it { should have_many(:tickets) }
    it 'should have 300 tickets' do
      expect(event_with_tickets.tickets.count).to eq(300)
    end
  end
end
