# frozen_string_literal: true

# require 'rails_helper'

# RSpec.describe User, type: :model do
#   let(:user) { create(:user) }
#   let(:user_with_tickets) { create(:user_with_tickets) }

#   describe 'validations' do
#     subject { user }
#     it { should validate_presence_of(:email) }
#     it { should validate_presence_of(:password) }
#     it { should validate_uniqueness_of(:email).case_insensitive }
#   end

#   describe "user's tickets" do
#     subject { user_with_tickets }
#     it { should have_many(:tickets) }
#     it 'should have five tickets' do
#       expect(user_with_tickets.tickets.count).to eq(5)
#     end
#   end
# end
