# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ticket, type: :model do
  let(:ticket) { create(:ticket) }

  describe 'validations' do
    subject { ticket }
    it { should belong_to(:event) }
    it { should belong_to(:user).optional }
  end
end
