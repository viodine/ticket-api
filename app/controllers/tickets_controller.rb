# frozen_string_literal: true

class TicketsController < ApplicationController
  include Adapters::Payment

  def create
    begin
      check_seats(params)
    rescue StandardError => e
      @message = e.message
    else
      sum = sum_price
      token = simulate_transaction_check
      pay(sum, token)
    end
  end

  private

  def check_seats(params)
    @tickets = []
    params[:tickets].each do |t|
      ticket = Ticket.find(t[:id])
      if ticket.is_paid == false
        @tickets << ticket
      else
        raise Class.new(StandardError), "Seat #{ticket.seat}, Row #{ticket.row} in sector #{ticket.sector} is unavailable. Try another one."
      end
    end
  end

  def sum_price
    @tickets.sum(&:price)
  end

  def simulate_transaction_check
    sleep(1)
    ['card_error', 'payment_error', ''].sample
  end

  def pay(sum, token)
    begin
      Gateway.charge(amount: sum, token: token)
    rescue StandardError => e
      @message = e.message
      @price = nil
    else
      @message = 'Transaction was successful.'
      @price = sum
      @tickets.each do |t|
        t.update(is_paid: true)
      end
    end
  end
end
