# frozen_string_literal: true

class EventsController < ApplicationController
  def index
    @events = Event.all
  end

  def show
    @event = Event.find(params[:id])
    @available = @event.tickets.select { |t| t.is_paid == false }
  end
end
