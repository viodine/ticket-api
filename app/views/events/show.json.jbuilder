# frozen_string_literal: true

json.event do
  json.id @event.id
  json.name @event.name
  json.datetime @event.datetime
  json.available @available.count
  json.tickets do
    json.partial! partial: 'tickets/ticket', collection: @available, as: :ticket
  end
end
