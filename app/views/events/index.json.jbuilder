# frozen_string_literal: true

json.array! @events do |event|
  json.id event.id
  json.name event.name
  json.datetime event.datetime
end
