# frozen_string_literal: true

json.info do
  json.message @message
  json.price @price
end
