# frozen_string_literal: true

json.id ticket.id
json.sector ticket.sector
json.row ticket.row
json.seat ticket.seat
json.price ticket.price
