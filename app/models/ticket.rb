# frozen_string_literal: true

class Ticket < ApplicationRecord
  validates :price, :event_id, presence: true
  belongs_to :event
  belongs_to :user, optional: true
end
