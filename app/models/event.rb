# frozen_string_literal: true

class Event < ApplicationRecord
  validates :name, :datetime, presence: true
  has_many :tickets
end
