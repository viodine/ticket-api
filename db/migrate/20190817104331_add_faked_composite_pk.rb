class AddFakedCompositePk < ActiveRecord::Migration[5.2]
  def change
    add_index :tickets, [:sector, :row, :seat, :event_id], :unique => true
  end
end
