class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.decimal :price, null: false
      t.string :sector
      t.integer :row
      t.integer :seat
      t.boolean :is_paid, null: false, default: false
      t.timestamps

      t.references :event
      t.references :user, optional: true
    end
  end
end
