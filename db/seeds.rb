# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do
  User.create!(email: Faker::Internet.email, password: Faker::Internet.password)
end

10.times do
  Event.create!(name: Faker::Music.band + ' Concert', datetime: Faker::Time.forward)
end

200.times do
  event = Event.all.sample
  Ticket.create!(price: Faker::Number.decimal(l_digits: 2, r_digits: 2),
                 sector: Faker::Types.character,
                 row: Faker::Number.between(from: 1, to: 50),
                 seat: Faker::Number.between(from: 1, to: 50),
                 event: event)
end
